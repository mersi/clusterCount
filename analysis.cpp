#include <TStyle.h>
#include <TGraphErrors.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TPaveStats.h>
#include <iostream>

TTree* myTree;
TTree* clusterSizeTree;
TCanvas* myCanvas;
TGraphErrors *clusterCountGraph, *clusterWidthGraph, *stripAddingGraph, *addingGraph, *hitStripGraph;
TH1D* clusterSizeHisto;
TF1 *clusterCountFunc, *clusterWidthFunc, *stripAddingFunc, *addingFunc, *hitStripFunc;

void setStatsGeom(TObject* aGraph, double x1, double y1, double x2, double y2) {
  auto aStats = (TPaveStats*) aGraph->FindObject("stats");
  if (!aStats) {
    std::cerr << "Missing stats" << std::endl;
    return;
  }
  aStats->SetX1NDC(x1);
  aStats->SetY1NDC(y1);
  aStats->SetX2NDC(x2);
  aStats->SetY2NDC(y2);
}

void analysis() {
  gStyle->SetOptFit(1111);
  myTree= new TTree();
  myTree->ReadFile("final.csv");
  const int N = myTree->GetEntries();


  // Useful for development
  if (gROOT->IsBatch()==false) myTree->Print();

  clusterSizeTree = new TTree();
  clusterSizeTree->ReadFile("cluster.dat");
  int    clusterSize;
  int maxClusterSize = 20;
  clusterSizeHisto = new TH1D("clusterSizeHisto", "Cluster size distribution (no overlaps);Cluster size / strips;Hits", maxClusterSize, 0.5, 0.5 + maxClusterSize);
  clusterSizeTree->SetBranchAddress("clusterSize", &clusterSize);
  int maxSeenSize = 0;
  for (auto iCluster = 0; iCluster<clusterSizeTree->GetEntries(); ++iCluster) {
    clusterSizeTree->GetEntry(iCluster);
    clusterSizeHisto->Fill(clusterSize);
    if (clusterSize>maxSeenSize) maxSeenSize = clusterSize;
  }
  if (maxSeenSize<7) maxSeenSize=7;
  clusterSizeHisto->GetXaxis()->SetRangeUser(0.5, maxSeenSize+0.5);
  clusterSizeHisto->SetNdivisions(maxSeenSize);



  // Variables to read the values
  int    nEvents;
  int    seen;
  int    generated;
  double average_occ;
  double seen_occ;
  double seen_occ_error;
  double av_seen;
  double av_seen_err;
  int    hitStrips;
  int    hitStrips_squared;
  double av_cluster_size;
  double av_cluster_size_err;
  double av_addedStrips;
  double av_addedStrips_err;
  double av_addedClusters;
  double av_addedClusters_err;
  int    nStrips;

  // Assign branch names to addresses
  myTree->SetBranchAddress("nEvents", &nEvents);
  myTree->SetBranchAddress("seen", &seen);
  myTree->SetBranchAddress("generated", &generated);
  myTree->SetBranchAddress("average_occ", &average_occ);
  myTree->SetBranchAddress("seen_occ", &seen_occ);
  myTree->SetBranchAddress("seen_occ_err", &seen_occ_error);
  myTree->SetBranchAddress("av_seen", &av_seen);
  myTree->SetBranchAddress("av_seen_err", &av_seen_err);
  myTree->SetBranchAddress("hitStrips", &hitStrips);
  myTree->SetBranchAddress("hitStrips_squared", &hitStrips_squared);
  myTree->SetBranchAddress("av_cluster_size", &av_cluster_size);
  myTree->SetBranchAddress("av_cluster_size_err", &av_cluster_size_err);
  myTree->SetBranchAddress("av_addedStrips", &av_addedStrips);
  myTree->SetBranchAddress("av_addedStrips_err", &av_addedStrips_err);
  myTree->SetBranchAddress("av_addedClusters", &av_addedClusters);
  myTree->SetBranchAddress("av_addedClusters_err", &av_addedClusters_err);
  myTree->SetBranchAddress("hitStrips", &hitStrips);
  myTree->SetBranchAddress("nStrips", &nStrips);

  // Create a graphs to hold the values, with their errors

  clusterCountGraph = new TGraphErrors();
  clusterCountGraph->SetTitle("Measured vs simulated cluster occupancy;Simulated N_{Hits}/N_{Strips} per module per event;Measured N_{Clusters}/N_{Strips} per module per event");
  clusterCountGraph->SetName("clusterCountGraph");
  clusterWidthGraph = new TGraphErrors();
  clusterWidthGraph->SetName("clusterWidthGraph");
  clusterWidthGraph->SetTitle("Cluster size vs occupancy;N_{Hits}/N_{Strips} per module per event;Average cluster size");
  stripAddingGraph = new TGraphErrors();
  stripAddingGraph->SetName("stripAddingGraph");
  stripAddingGraph->SetTitle("Strip increment;Cluster width (strips);Average added strips per hit");
  addingGraph = new TGraphErrors();
  addingGraph->SetName("addingGraph");
  addingGraph->SetTitle("Merging probability;Cluster width (strips);Average added clusters per hit");
  hitStripGraph = new TGraphErrors();
  hitStripGraph->SetName("hitStripGraph");
  hitStripGraph->SetTitle("Hit strips vs occupancy;Simulated N_{Hits}/N_{Strips} per module per event;Number of hit strips");

  double yval, variance;
  for (int k=0;k<N;k++) {
   myTree->GetEntry(k);
   clusterCountGraph->SetPoint(k, average_occ, av_seen);
   clusterCountGraph->SetPointError(k, 0, av_seen_err);

   clusterWidthGraph->SetPoint(k, average_occ, av_cluster_size);
   clusterWidthGraph->SetPointError(k, 0, av_cluster_size_err);

   yval = double(hitStrips);
   variance = sqrt(hitStrips);
   if (variance==0) variance=1;
   yval /= nEvents*nStrips;
   variance /= nEvents*nStrips;

   hitStripGraph->SetPoint(k, average_occ, yval);
   hitStripGraph->SetPointError(k, 0, variance);
  }

  std::string fitOpt = "";
  if (gROOT->IsBatch()) fitOpt = "Q";


  myCanvas = new TCanvas("fitCanvas", "Fit Canvas", 1000, 1000);
  myCanvas->Divide(2,2);
  myCanvas->cd(1);
  myCanvas->GetPad(1)->SetLeftMargin(0.15);
  clusterSizeHisto->Draw();


  myCanvas->cd(2);
  myCanvas->GetPad(2)->SetLeftMargin(0.15);
  myCanvas->GetPad(2)->SetLogx();
  clusterWidthGraph->Draw("ap");
  clusterWidthFunc = new TF1("clusterWidthFunc", "[0]*exp([1]*x)", 0, 1);
  clusterWidthFunc->SetParameter(0,1);
  clusterWidthFunc->SetParameter(1,1);
  clusterWidthGraph->Fit(clusterWidthFunc, fitOpt.c_str(), "", 0, 1);

  double alpha = clusterWidthFunc->GetParameter(0);
  
  myCanvas->cd(3);
  myCanvas->GetPad(3)->SetLeftMargin(0.15);
  hitStripGraph->Draw("ap");
  hitStripFunc = new TF1("hitStripFunc","1-exp(-[0]*x)" , 0 ,1);
  hitStripGraph->Fit(hitStripFunc, fitOpt.c_str(), "", 0, 1);

  myCanvas->cd(4);
  myCanvas->GetPad(4)->SetLeftMargin(0.15);
  myCanvas->GetPad(4)->SetLogx();
  myCanvas->GetPad(4)->SetLogy();
  clusterCountGraph->Draw("ap");
  clusterCountFunc = new TF1("clusterCountFunc", " - exp(-([0]+1)*x) + exp(-[0]*x)", 0, 1);

  clusterCountFunc->SetParameter(0, alpha);
  clusterCountGraph->Fit(clusterCountFunc, fitOpt.c_str(), "", 0, 1);

  double newClusterSize = clusterWidthFunc->GetParameter(0);
  for (int k=0;k<N;k++) {
   myTree->GetEntry(k);
   stripAddingGraph->SetPoint(k, av_cluster_size, av_addedStrips);
   stripAddingGraph->SetPointError(k, 0, av_addedStrips_err);

   // merging probability
   addingGraph->SetPoint(k, seen_occ, (1-av_addedClusters)/(1+newClusterSize+av_cluster_size));
  }

  // myCanvas->cd(4);
  // stripAddingGraph->Draw("ap");
 

  //myCanvas->cd(6);
  //addingGraph->SetMarkerStyle(8);
  //addingGraph->Draw("ap");
  //addingGraph->Fit("pol1");
  /*
  addingFunc = new TF1("addingFunc", "[4]*exp([3]*x*x*x+[2]*x*x+[1]*x+[0])", 0, 100);
  addingFunc->SetParameter(0, 30.724);
  addingFunc->SetParameter(1, -0.0395);
  addingFunc->SetParameter(2, 0.013);
  addingFunc->SetParameter(3, -0.001684);
  addingFunc->SetParameter(4, 0.000202068);
  addingGraph->Fit(addingFunc, "", 0, 1);
 */


  std::cout << "HEADER/C,"
    << "width_p0/D" << ","
    << "width_p0_err/D" << ","
    << "width_p1/D" << ","
    << "width_p1_err/D" << ","
    << "width_chi2/D" << ","
    << "width_ndf/D" << ","
    << "cluster_p0/D" << ","
    << "cluster_p0_err/D" << ","
    << "cluster_chi2/D" << ","
    << "cluster_ndf/D" << std::endl;

  std::cout << "RESULT,"
    << clusterWidthFunc->GetParameter(0) << ","
    << clusterWidthFunc->GetParError(0)  << ","
    << clusterWidthFunc->GetParameter(1) << ","
    << clusterWidthFunc->GetParError(1)  << ","
    << clusterWidthFunc->GetChisquare()  << ","
    << clusterWidthFunc->GetNDF()        << ","
    << clusterCountFunc->GetParameter(0) << ","
    << clusterCountFunc->GetParError(0)  << ","
    << clusterCountFunc->GetChisquare()  << ","
    << clusterCountFunc->GetNDF()        << std::endl;

  myCanvas->Update();

  setStatsGeom(clusterSizeHisto,  0.56, 0.70, 0.88, 0.88);
  setStatsGeom(clusterWidthGraph, 0.17, 0.70, 0.60, 0.88);
  setStatsGeom(hitStripGraph,     0.17, 0.73, 0.60, 0.88);
  setStatsGeom(clusterCountGraph, 0.17, 0.73, 0.60, 0.88);

  myCanvas->SaveAs(Form("debugPlots/fit_%f.pdf", clusterWidthFunc->GetParameter(0)));

  if (gROOT->IsBatch()) gSystem->Exit(0);

}
