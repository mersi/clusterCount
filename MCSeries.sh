#!/bin/bash

i=0.5
j=1
while ((j<6)); do
  >&2 echo "i = $i"
  i=`bc <<< "$i+0.2"`
  j=`echo $i | cut -d'.' -f1`
  ./merging $i | pv --size 2360 - > final.csv && root -l -b analysis.cpp
done > temp.csv
egrep '^HEADER' temp.csv | head -n1 > meta.csv
egrep '^RESULT' temp.csv  >> meta.csv

root -l metaAnalysis.cpp

