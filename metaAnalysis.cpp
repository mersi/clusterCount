
TTree *fits;
TCanvas *myCanvas;
TGraph *myGraph;

void metaAnalysis() {
  fits = new TTree();
  myCanvas = new TCanvas();
  myCanvas->cd();
  fits->ReadFile("meta.csv");
  fits->Draw("width_p1:width_p0");
  myGraph = (TGraph*)myCanvas->GetPrimitive("Graph")->Clone();
  myGraph->SetMarkerStyle(8);
  myGraph->Draw("ap");
  TF1* myFunc = new TF1("myFunc", "exp([0]+[1]*x+[2]*x*x)", 0, 1);
  myGraph->Fit(myFunc);
}

