#include <iostream>
#include <cstdio>
#include <ctime>
#include <cmath>
#include <fstream>
#include <string>
#include <map>

const unsigned int nStrips = 1016;
int layer[nStrips] = {0};
unsigned int hitStrips = 0;

enum class ClusterWidthModel : int {constant = 0, uniform = 1, exponential = 2, realistic = 3 , error = -1};
std::map<std::string, ClusterWidthModel> ClusterWidthModelDictionary;

void initClusterWidthModelDictionary() {
  ClusterWidthModelDictionary["constant"] = ClusterWidthModel::constant;
  ClusterWidthModelDictionary["c"] = ClusterWidthModel::constant;
  ClusterWidthModelDictionary["uniform"] = ClusterWidthModel::uniform;
  ClusterWidthModelDictionary["u"] = ClusterWidthModel::uniform;
  ClusterWidthModelDictionary["exponential"] = ClusterWidthModel::exponential;
  ClusterWidthModelDictionary["e"] = ClusterWidthModel::exponential;
  ClusterWidthModelDictionary["realistic"] = ClusterWidthModel::realistic;
  ClusterWidthModelDictionary["r"] = ClusterWidthModel::realistic;
}

ClusterWidthModel getClusterWidthModel(std::string command) {
  for (auto it : ClusterWidthModelDictionary) {
    if (it.first == command) return ClusterWidthModelDictionary[command];
  }
  return ClusterWidthModel::error;
}

std::ofstream *datFile = nullptr;
std::ofstream *clusterFile = nullptr;
int clusterSaveCount = 1e6;

class SimResult {
  public:
    unsigned int generated = 0;
    unsigned int seen = 0;
    unsigned int strips = 0;
    unsigned int addedClusters = 0;
    unsigned int addedStrips = 0;
};

class DataSeries {
public:
  DataSeries();
  double sum;
  double squares_sum;
  double N;
  void add(double x);
  double getMean();
  double getError();
};

class SimParams {
public:
  unsigned int nEvents = 1000;
  unsigned int nRepeats = 10;
  double minOccupancy = 1e-3;
  double maxOccupancy = 0.5;
  double percentOccupancyStep = 5;
  double widthScale = 2;
  ClusterWidthModel widthModel = ClusterWidthModel::constant;
};

void clearLayer() {
  for (auto i=0; i<nStrips; ++i) {
    layer[i] = 0;
  }
  hitStrips = 0;
}

void addHit(unsigned int x, unsigned int w) {
  for (auto i=0; i<w; ++i) {
    if (layer[x+i]==0) {
      hitStrips++;
      layer[x+i]=1;
    }
  }
}

unsigned int countClusters() {
  int status=0;
  int inCluster=0;
  unsigned int nClusters = 0;
  for (auto i=0; i<nStrips; ++i) {
    if (layer[i]>0) {
      if (inCluster==0) {
        nClusters++;
      }
      inCluster=1;
    } else {
      inCluster=0;
    }
  }
  return nClusters;
}

double dice() {
  long double random = rand();
  random /= RAND_MAX;
  return(random);
}

int dice_int(int faces) {
  double random = dice();
  random *= faces;
  return int(random);
}

double extract_step(double lambda) {
  return -log(1-dice())/lambda;
}

void testClusters() {
  clearLayer();
  addHit(0, 1);
  std::cout << countClusters() << std::endl;
  addHit(1, 1);
  std::cout << countClusters() << std::endl;

  addHit(10, 1);
  std::cout << countClusters() << std::endl;
  addHit(11, 1);
  std::cout << countClusters() << std::endl;
}

void testExtract() {
  std::cout << "x/D" << std::endl;
  for (int i=0; i<1e5; ++i) {
    std::cout << extract_step(3) << std::endl;
  }
}

void testDice() {
  // generate random number
  int count[4] = {0};
  for (int i=0; i<100000; ++i) {
    count[dice_int(4)]++;
  }
  for (int i=0; i<4; ++i) {
    std::cout << i << " -> " << count[i] << std::endl;
  }
}

int generateClusterWidthRealistic() {
  double x = dice();
  if (x<.5) return 1;
  else if (x<.5+.35) return 2;
  else return 3;
}

int generateClusterWidthExpo(double widthScale) {
  double w = extract_step(1)*(widthScale-0.5);
  return w+1;
}

int generateClusterWidthUniform(double widthScale) {
  return int(dice()*(2*widthScale-1))+1;
}

int generateClusterWidth(SimParams& p) {
  int result;

  if (p.widthModel == ClusterWidthModel::constant) {
    result = p.widthScale;
  } else if (p.widthModel == ClusterWidthModel::uniform) {
    result = generateClusterWidthUniform(p.widthScale);
  } else if (p.widthModel == ClusterWidthModel::exponential) {
    result = generateClusterWidthExpo(p.widthScale);
  } else if (p.widthModel == ClusterWidthModel::realistic) {
    result = generateClusterWidthRealistic();
  } else {
    result = -1;
    std::cerr << "ERROR: unknown cluster size model selected. This should not happen." << std::endl;
  }

  if (clusterSaveCount>0) {
    clusterSaveCount--;
    *clusterFile << result << std::endl;
  }
  return result;
}

void testClusterWidth() {
  SimParams p;
  int count[4] = {0};
  for (int i=0; i<100000; ++i) {
    count[generateClusterWidth(p)]++;
  }
  for (int i=0; i<4; ++i) {
    std::cout << i << " -> " << count[i] << std::endl;
  }
}

unsigned int generateClusters(SimParams& p, double lambda) {
  double x;
  double w;
  unsigned int generated = 0;
  for (auto i=extract_step(lambda); i<nStrips; i+=extract_step(lambda)) {
    generated++;
    w=generateClusterWidth(p);
    addHit(i, w);
  }
  return generated;
}

unsigned int simulateEvent(SimParams& p, double avClusters) {
  clearLayer();
  double lambda = avClusters / nStrips;
  return generateClusters(p, lambda);
}

void testGeneration(double avClusters = 3) {
  unsigned int generated;
  unsigned int seen;
  SimParams p;

  std::cout << "Average/D,Generated/D,Seen/D" << std::endl;
  for (int i=0; i<1e5; ++i) {
    generated = simulateEvent(p, avClusters);
    seen = countClusters();
    std::cout << hitStrips << ",";
    std::cout << avClusters << ",";
    std::cout << generated <<",";
    std::cout << seen << std::endl;
  }
}

SimResult simulateEvents(SimParams& p, double avClusters) {
  SimResult result;

  for (unsigned int i=0; i<p.nEvents; ++i) {
    result.generated += simulateEvent(p, avClusters);
    auto beforeSeen = countClusters();
    result.seen += beforeSeen; 
    result.strips += hitStrips;
    auto beforeHitStrips = hitStrips;

    // Incremental prob
    addHit(100, generateClusterWidth(p));
    auto addedClusters = countClusters();
    result.addedClusters += addedClusters - beforeSeen;
    result.addedStrips += (hitStrips - beforeHitStrips);
  }
  return result;
}

DataSeries::DataSeries() {
  N=0;
  sum=0;
  squares_sum=0;
}

void DataSeries::add(double x) {
  sum += x;
  squares_sum += x*x;
  N++;
};

double DataSeries::getMean() {
  return sum/N;
}

double DataSeries::getError() {
  if (N<=1) {
    std::cerr << "ERROR: trying to do stats with 1 sample!" << std::endl;
    return 0;
  }
  double rms = squares_sum/N - pow(sum/N, 2);
  rms = sqrt(rms);
  if (rms<=0) rms=1;
  return rms/sqrt(N-1);
}

void simulateClusterGeneration(SimParams& p) {
  std::cout << "nEvents/I,nStrips/I,hitStrips/I,average/D,generated/I,seen/I,seen_err/D,av_seen/D,av_seen_err/D,average_occ/D,generated_occ/D,seen_occ/D,seen_occ_err/D,av_cluster_size/D,av_cluster_size_err/D,av_addedClusters/D,av_addedClusters_err/D,av_addedStrips/D,av_addedStrips_err/D" << std::endl;
  if (p.nRepeats<=1) {
    std::cout << "ERROR: we need at least 2 repeats to evaluate the error" << std::endl;
  }
  double start = p.minOccupancy*nStrips;
  double stop = p.maxOccupancy*nStrips;
  double step = 1+p.percentOccupancyStep/100;

  for (double avClusters=start; avClusters<stop; avClusters *= step) {
    SimResult result, oneResult;
    double average, rms, error;

    DataSeries size, count, seen, addedClusters, addedStrips;
    for (int i=0; i<p.nRepeats; ++i) {
      oneResult = simulateEvents(p, avClusters);
      result.strips += oneResult.strips;
      result.generated += oneResult.generated;
      result.seen += oneResult.seen;

      size.add(double(oneResult.strips)/oneResult.seen);
      count.add(double(oneResult.seen)/p.nEvents/nStrips);
      seen.add(oneResult.seen);
      addedClusters.add(oneResult.addedClusters);
      addedStrips.add(oneResult.addedStrips);

    }
    std::cout << p.nEvents*p.nRepeats << ",";
    std::cout << nStrips << ",";
    std::cout << result.strips << ",";
    std::cout << avClusters << ",";
    std::cout << result.generated << ",";
    std::cout << result.seen << ",";
    std::cout << sqrt(result.seen) << ",";
    std::cout << seen.getMean()/p.nEvents/nStrips << ",";
    std::cout << seen.getError()/p.nEvents/nStrips << ",";
    std::cout << avClusters/nStrips << ",";
    std::cout << double(result.generated)/p.nEvents/p.nRepeats/nStrips << ",";
    std::cout << count.getMean() << ",";
    std::cout << count.getError() << ",";
    std::cout << size.getMean() << ",";
    std::cout << size.getError() << ",";
    std::cout << addedClusters.getMean()/p.nEvents << ",";
    std::cout << addedClusters.getError()/p.nEvents << ",";
    std::cout << addedStrips.getMean()/p.nEvents << ",";
    std::cout << addedStrips.getError()/p.nEvents << std::endl;
  }
}

SimResult testSimulateEvents(unsigned int nEvents, unsigned int nClusters) {
  SimParams p;
  p.nEvents = nEvents;
  auto result = simulateEvents(p, 1);
  std::cout << nEvents << " events with " << nClusters << " cluster" << std::endl;
  std::cout << "strips = " << result.strips << std::endl;
  std::cout << "seen = " << result.seen << std::endl;
  return result;
}

void testSimulateEventsRepeated(unsigned int nRepeats, unsigned int nEvents, unsigned int nClusters) {
  SimResult result;
  double mean, rms, error;

  std::cout << "strips/I,seen/I,mean/D" << std::endl;
  for (int i=0; i<nRepeats; ++i) {
    result = testSimulateEvents(nEvents,nClusters);
    std::cout << result.strips << ",";
    std::cout << result.seen << ",";
    mean = double(result.strips)/result.seen;
    std::cout << mean << ",";
  }
}

int main(int argc, char* argv[]) {
  SimParams p;

  if (argc==2) {
    p.widthScale = atof(argv[1]);
  } else if (argc==3) {
    initClusterWidthModelDictionary();
    p.widthScale = atof(argv[1]);
    p.widthModel = getClusterWidthModel(argv[2]);
    if (p.widthModel == ClusterWidthModel::error) {
      std::cerr << "ERROR: unknown cluster size model '" << argv[2] << "'" << std::endl;
      std::cerr << "Allowed values are:" << std::endl;
      for (auto it : ClusterWidthModelDictionary) {
        std::cerr << " - " << it.first << std::endl;
      }
      return -1;
    }
  }

  clusterFile = new std::ofstream("cluster.dat");
  *clusterFile << "clusterSize/I" << std::endl;

  srand(time(0));
  p.nEvents = 1000;
  p.nRepeats = 10;
  p.minOccupancy = 1e-4;
  p.maxOccupancy = 0.2;
  p.percentOccupancyStep = 5;

  simulateClusterGeneration(p);

  clusterFile->close();
  return 0;
}
